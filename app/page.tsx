"use client";

import Badge from "@/components/badge";
import Image from "next/image";
import { IoSchool } from "react-icons/io5";
import {
  FaDiceD6,
  FaDiscord,
  FaGithub,
  FaLaptopCode,
  FaLink,
  FaLinkedin,
} from "react-icons/fa";
import { FaFilePen, FaGear, FaGitlab, FaLocationDot } from "react-icons/fa6";
import { MdEmail, MdOutlineWork } from "react-icons/md";
import { Typewriter } from "react-simple-typewriter";
import styles from "./page.module.scss";
import sectionStyles from "@/styles/section.module.scss";
import classNames from "classnames";

const Home: React.FC = () => {
  return (
    <main className={sectionStyles.main}>
      <header className={styles.headRoot}>
        <div className={styles.headCenter}>
          <Image
            className={styles.photo}
            src="/pictures/avatar.gif"
            alt="Photo"
            width="180"
            height="180"
            priority
            unoptimized
          />
          <div className={styles.content}>
            <div className={styles.nameAndBadges}>
              <h1>Kylian Prévot</h1>
              <div className={styles.badges}>
                <Badge badgeIcon={<FaLocationDot />} tooltipText="France" />
                <Badge badgeIcon={<FaLaptopCode />} tooltipText="Développeur" />
                <Badge
                  badgeIcon={<IoSchool />}
                  tooltipText="3e année de BUT informatique"
                />
              </div>
            </div>
            <div className={styles.typewriter}>
              <Typewriter
                words={[
                  "Développeur.",
                  "Junior réseau et système.",
                  "Curieux, passionné et ambitieux.",
                  "Passionné d'audiovisuel et d'animation.",
                ]}
                cursor
                loop={0}
              />
            </div>
          </div>
        </div>
      </header>
      <section className={sectionStyles.sectionRoot}>
        <section
          className={classNames(
            sectionStyles.section,
            sectionStyles.textSection
          )}
        >
          <h1>
            <FaFilePen />
            Présentation
          </h1>
          <p>
            Je me prénomme Kylian Prévot et suis un jeune étudiant de 20 ans en
            3e année de BUT informatique à Orléans. Passionné dès mes premières
            années de collège, j&apos;ai expérimenté divers domaines en
            apprenant et pratiquant en auto-didacte sur mon temps libre. Etant
            pourtant axé sur le hardware / l&apos;électronique à l&apos;origine,
            je me suis progressivement orienté vers la programmation
            informatique.
          </p>
          <p>
            Je suis aujourd&apos;hui capable de développer en Java, Python,
            HTML, CSS, Javascript / Typescript, et également faire du
            développement web avec React et Vue.js. D&apos;ailleurs, il
            s&apos;agit du domaine qui me captive le plus et dans lequel je
            souhaiterai faire carrière. De plus, je gère et maintiens une
            infrastructure auto-hébergée, constituée principalement d&apos;un
            hyperviseur sous Proxmox, qui me permet d&apos;héberger différents
            services et de les isoler à travers différentes machines virtuelles.
          </p>
          <p>
            Vous trouverez dans ce portfolio mon parcours scolaire /
            universitaire et professionnel, les différentes bases et compétences
            que je possède, mes centres d&apos;intérêt ainsi que les divers
            moyen de me contacter. En vous souhaitant une bonne lecture !
          </p>
        </section>
        <div className={styles.sectionGroup}>
          <section className={sectionStyles.section}>
            <h1>
              <IoSchool />
              Parcours scolaire et universitaire
            </h1>
            <div className={styles.timeline}>
              <div className={styles.timelineItem}>
                <h3>BUT Informatique</h3>
                <p>
                  <FaLocationDot /> IUT Informatique d&apos;Orléans (2021-2024)
                </p>
                <div className={styles.timelineItemContent}>
                  <p>
                    Parcours développement,
                    conception et validation.
                  </p>
                  <p>
                    Formation assez diversifiée dont la finalité est de rentrer
                    directement dans la vie active, ou de poursuivre ses études
                    dans le développement logiciel ou le développement web.
                  </p>
                  <p>
                    D&apos;autres notions y sont également introduites, comme du
                    réseau, de la conteneurisation / virtualisation, ou encore
                    de l&apos;intelligence artificielle.
                  </p>
                </div>
              </div>
              <div className={styles.timelineItem}>
                <h3>Baccalauréat général</h3>
                <p>
                  <FaLocationDot /> Lycée privé Saint-François de Sales, Gien
                  (2018 - 2021)
                </p>
                <div className={styles.timelineItemContent}>
                  <p>
                    Bac obtenu durant la pandémie de COVID-19, avec mention
                    assez bien.
                  </p>
                  <ul>
                    <li>Spécialités Mathématiques et Physique-Chimie</li>
                    <li>Option Mathématiques Expertes</li>
                    <li>Section européenne (Anglais)</li>
                  </ul>
                </div>
              </div>
              <div className={styles.timelineItem}>
                <h3>Brevet des collèges</h3>
                <p>
                  <FaLocationDot /> Collège Gérard Philipe, Aubigny-sur-Nère
                  (2014 - 2018)
                </p>
                <div className={styles.timelineItemContent}>
                  <p>Brevet obtenu avec mention très bien.</p>
                  <p>LV1 Anglais et LV2 Allemand.</p>
                </div>
              </div>
            </div>
          </section>
          <section className={sectionStyles.section}>
            <h1>
              <MdOutlineWork />
              Parcours professionnel
            </h1>
            <div className={styles.timeline}>
              <div className={styles.timelineItem}>
                <h3>Stage de 3e année de BUT</h3>
                <p>
                  <FaLocationDot /> CHU d&apos;Orléans (février à juin 2024)
                </p>
                <div className={styles.timelineItemContent}>
                  <p>
                    Stage de mise en pratique d&apos;une durée de 16 semaines,
                    effectué dans le cadre de la 3e année du BUT Informatique.
                  </p>
                  <ul>
                    <li>
                      Adaptation / amélioration du travail effectué durant le
                      stage précédent, pour tous les établissements du GHT du
                      Loiret
                    </li>
                    <li>Mise en production</li>
                    <li>Assistance auprès des utilisateurs</li>
                    <li>Mesure du taux d&apos;utilisation de la solution</li>
                  </ul>
                </div>
              </div>
              <div className={styles.timelineItem}>
                <h3>Stage de 2e année de BUT</h3>
                <p>
                  <FaLocationDot />
                  Centre Hospitalier Pierre Dezarnaulds, Gien (avril à juin
                  2023)
                </p>
                <div className={styles.timelineItemContent}>
                  <p>
                    Stage de mise en pratique d&apos;une durée de 8 semaines,
                    effectué dans le cadre de la 2e année du BUT Informatique.
                  </p>
                  <p>
                    Développement d&apos;un site web d&apos;assistance
                    utilisateur pour un accompagnement à la gestion de mots de
                    passe.
                  </p>
                  <ul>
                    <li>
                      Analyse du besoin et proposition d&apos;une solution
                    </li>
                    <li>Développement de la solution</li>
                    <li>Mise en production</li>
                  </ul>
                </div>
              </div>
              <div className={styles.timelineItem}>
                <h3>Stage d&apos;observation</h3>
                <p>
                  <FaLocationDot /> Hôtel de ville d&apos;Aubigny-sur-Nère
                  (2018)
                </p>
                <div className={styles.timelineItemContent}>
                  <p>
                    Stage de 3e, d&apos;une durée d&apos;une semaine, au sein du
                    service informatique de l&apos;Hôtel de ville.
                  </p>
                  <ul>
                    <li>
                      Gestion / Maintenance d&apos;un parc informatique complexe
                    </li>
                    <li>Intervetions sur le terrain</li>
                    <li>Dépannage informatique</li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
        <section className={sectionStyles.section}>
          <h1>
            <FaGear />
            Compétences
          </h1>
          <div className={styles.sectionGroup}>
            <section className={styles.skills}>
              <h2>Acquis</h2>
              <p>
                Languages de programmation, outils, notions que j&apos;utilise
                le plus régulièrement et / ou avec lesquels je possède le plus
                d&apos;expérience.
              </p>
              <div className={styles.tags}>
                <span>Java</span>
                <span>Python</span>
                <span>HTML</span>
                <span>CSS</span>
                <span>Javascript / Typescript</span>
                <span>React</span>
                <span>Vue.js</span>
                <span>Bash</span>
                <span>PowerShell</span>
                <span>Docker</span>
                <span>Windows</span>
                <span>GNU / Linux</span>
                <span>SQL</span>
                <span>Git</span>
              </div>
            </section>
            <section className={styles.skills}>
              <h2>Junior</h2>
              <p>
                Languages de programmation, outils, notions pour lesquels je
                possède des bases théoriques et pour lesquels mon expérience
                doit davantage se développer.
              </p>
              <div className={styles.tags}>
                <span>PHP</span>
                <span>Go</span>
                <span>Virtualisation</span>
                <span>Réseau</span>
                <span>Système</span>
                <span>DevOps</span>
                <span>Encodage vidéo</span>
                <span>Filtrage vidéo</span>
              </div>
            </section>
          </div>
        </section>
        <section className={styles.sectionGroup}>
          <section className={sectionStyles.section}>
            <h1>
              <FaDiceD6 />
              Centres d&apos;intérêt
            </h1>
            <div className={styles.tags}>
              <span>Hardware</span>
              <span>GNU / Linux</span>
              <span>Développement web</span>
              <span>SysAdmin</span>
              <span>Musique</span>
              <span>Audiovisuel</span>
              <span>Encodage vidéo</span>
              <span>Animation</span>
              <span>Animation japonaise</span>
              <span>Bandes dessinées</span>
              <span>Créations DIY</span>
              <span>Infrastructures routières</span>
            </div>
          </section>
          <section className={sectionStyles.section}>
            <h1>
              <FaLink />
              Mes différents contacts
            </h1>
            <div className={styles.tags}>
              <a href="mailto:contact@kylianprevot.fr">
                <MdEmail />
                E-mail
              </a>
              <a
                href="https://www.linkedin.com/in/kylian-pr%C3%A9vot-404834293/"
                target="_blank"
              >
                <FaLinkedin />
                LinkedIn
              </a>
              <a
                href="https://discord.com/users/537979556259430411/"
                target="_blank"
              >
                <FaDiscord />
                Discord
              </a>
              <a href="https://github.com/pkylian/" target="_blank">
                <FaGithub />
                GitHub
              </a>
              <a href="https://gitlab.com/kylianp/" target="_blank">
                <FaGitlab />
                GitLab
              </a>
            </div>
          </section>
        </section>
      </section>
    </main>
  );
};

export default Home;
