import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.scss";
import Footer from "@/components/footer";
import classNames from "classnames";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Kylian Prévot - Portfolio",
  description:
    "Je me prénomme Kylian Prévot, j'ai 20 ans et je suis passionné par l'informatique. Dans ce portfolio, vous trouverez une présentation personnelle, ainsi que mes différentes expériences et compétences. Bonne lecture !",
  creator: "Kylian Prévot",
  authors: [{ name: "Kylian Prévot", url: "https://kylianprevot.fr/" }],
  keywords: [
    "portfolio",
    "présentation",
    "études",
    "parcours",
    "université",
    "expérience",
    "expériences",
    "compétences",
  ],
  openGraph: {
    type: "website",
    countryName: "France",
    url: "https://kylianprevot.fr/",
    siteName: "Kylian Prévot - Portfolio",
    title: "Kylian Prévot - Portfolio",
    description:
      "Je me prénomme Kylian Prévot, j'ai 20 ans et je suis passionné par l'informatique. Dans ce portfolio, vous trouverez une présentation personnelle, ainsi que mes différentes expériences et compétences. Bonne lecture !",
    images: "https://kylianprevot.fr/pictures/avatar.gif",
    emails: "contact@kylianprevot.fr",
  },
};

const RootLayout = ({ children }: Readonly<{ children: React.ReactNode }>) => {
  return (
    <html lang="en" suppressHydrationWarning>
      <body className={classNames(inter.className, "app-root")}>
        {children}
        <Footer />
      </body>
    </html>
  );
};

export default RootLayout;
