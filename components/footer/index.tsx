"use client";

import styles from "./styles.module.scss";
import { useCallback, useState } from "react";
import Modal from "../modal";
import {
  FaCloudflare,
  FaFonticons,
  FaLaptopCode,
  FaNetworkWired,
  FaServer,
} from "react-icons/fa";
import { FaGitlab } from "react-icons/fa6";
import { TbWorldWww } from "react-icons/tb";

const Footer: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  const openLegalMentions = useCallback((): void => {
    setOpen(true);
  }, []);

  return (
    <>
      <footer className={styles.footer}>
        <p>
          © {new Date().getFullYear()} Kylian Prévot. Tous droits réservés.{" "}
          <button onClick={openLegalMentions}>Mentions légales</button>.
        </p>
      </footer>
      <Modal
        open={open}
        setOpen={setOpen}
        title="Mentions légales"
        bodyClassName={styles.modalBody}
      >
        <div className={styles.modalSection}>
          <h2>
            <FaLaptopCode />
            Développeur
          </h2>
          <p>Kylian Prévot</p>
        </div>
        <div className={styles.modalSection}>
          <h2>
            <FaGitlab />
            Dépôt GitLab
          </h2>
          <a href="https://gitlab.com/kylianp/kylianprevot.fr" target="_blank">
            gitlab.com/kylianp/kylianprevot.fr
          </a>
        </div>
        <div className={styles.modalSection}>
          <h2>
            <FaServer />
            Hébergement
          </h2>
          <p>Site auto-hébergé</p>
        </div>
        <div className={styles.modalSection}>
          <h2>
            <FaNetworkWired />
            Fournisseur d&apos;accès à Internet
          </h2>
          <a href="https://www.bouyguestelecom.fr" target="_blank">
            Bouygues Telecom
          </a>
        </div>
        <div className={styles.modalSection}>
          <h2>
            <TbWorldWww />
            Fournisseur de nom de domaine
          </h2>
          <a href="https://www.ovhcloud.com" target="_blank">
            OVH
          </a>
        </div>
        <div className={styles.modalSection}>
          <h2>
            <FaCloudflare />
            Gestion du nom de domaine
          </h2>
          <p>
            <a href="https://cloudflare.com" target="_blank">
              Cloudflare
            </a>
          </p>
        </div>
        <div className={styles.modalSection}>
          <h2>
            <FaFonticons />
            Icônes utilisées
          </h2>
          <a href="https://react-icons.github.io/react-icons" target="_blank">React Icons</a>
        </div>
      </Modal>
    </>
  );
};

export default Footer;
