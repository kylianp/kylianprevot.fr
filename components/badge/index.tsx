import styles from "./styles.module.scss";

interface BadgeProps {
  badgeIcon: React.ReactNode;
  tooltipText: string;
}

const Badge: React.FC<BadgeProps> = ({ badgeIcon, tooltipText }) => {
  return (
    <div className={styles.badge}>
      <span className={styles.tooltip}>{tooltipText}</span>
      {badgeIcon}
    </div>
  );
};

export default Badge;
