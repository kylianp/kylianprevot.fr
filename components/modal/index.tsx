import {
  AllHTMLAttributes,
  DetailedHTMLProps,
  FC,
  useCallback,
  useEffect,
  useState,
} from "react";
import styles from "./styles.module.scss";
import { MdClose } from "react-icons/md";
import classNames from "classnames";

interface ModalProps {
  title: string;
  open: boolean;
  setOpen: (open: boolean) => void;
  bodyClassName: string;
}

const Modal: FC<
  DetailedHTMLProps<AllHTMLAttributes<HTMLDivElement>, HTMLDivElement> &
    ModalProps
> = ({ open, setOpen, title, bodyClassName, children }) => {
  const [isModalOpen, setModalIsOpen] = useState<boolean>(false);
  const closeModal = useCallback((): void => {
    setModalIsOpen(false);

    setTimeout((): void => setOpen(false), 300);
  }, [setOpen]);
  const closeModalWithEscape = useCallback(
    (event: KeyboardEvent): void => {
      if (event.key === "Escape") {
        closeModal();
      }
    },
    [closeModal]
  );

  useEffect((): void => {
    if (open) {
      setModalIsOpen(true);
    }
  }, [open]);

  useEffect((): (() => void) => {
    if (typeof window !== "undefined") {
      window.addEventListener("keydown", closeModalWithEscape);
    }

    return (): void => {
      if (typeof window !== "undefined") {
        window.removeEventListener("keydown", closeModalWithEscape);
      }
    };
  }, [closeModalWithEscape]);

  if (!open) {
    return null;
  }

  return (
    <div
      className={classNames(styles.modalRoot, { [styles.opened]: isModalOpen })}
      onClick={closeModal}
    >
      <div
        className={styles.modal}
        onClick={(event) => event.stopPropagation()}
      >
        <div className={styles.head}>
          <h1>{title}</h1>
          <MdClose
            title="Fermer la modale"
            aria-label="Fermer la modale"
            onClick={closeModal}
          />
        </div>
        <div className={classNames(styles.body, bodyClassName)}>{children}</div>
      </div>
    </div>
  );
};

export default Modal;
