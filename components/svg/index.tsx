import Books from "./books";
import Laptop from "./laptop";
import Location from "./location";

export { Books, Laptop, Location };
