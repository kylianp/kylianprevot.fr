const Location: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      viewBox="0 0 36 36"
    >
      <ellipse cx="18" cy="34.5" fill="#292f33" rx="4" ry="1.5" />
      <path
        fill="#99aab5"
        d="M14.339 10.725S16.894 34.998 18.001 35c1.106.001 3.66-24.275 3.66-24.275z"
      />
      <circle cx="18" cy="8" r="8" fill="#dd2e44" />
    </svg>
  );
};

export default Location;
